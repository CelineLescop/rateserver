import { Module } from '@nestjs/common';
import { WebModule } from './web/web.module';
import { Api1Module } from './api1/api1.module';
import { Api2Module } from './api2/api2.module';
import { DefaultController } from './web/default/default.controller';

@Module({
  imports: [WebModule, Api1Module, Api2Module],
  controllers: [DefaultController],
  providers: []
})
export class AppModule {}
