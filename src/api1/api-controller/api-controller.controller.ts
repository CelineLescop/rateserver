import { Controller, Get, Header, Render } from '@nestjs/common';
import { join } from 'path';
import { promises as fs } from 'fs'

@Controller('api/1')
export class ApiControllerController {

    @Get('/rates')
    @Header('Content-Type', 'application/json')
    async rates() {
        // join() assemble plusieurs chaîne pour en faire un chemin 
        // (et ajoute les / intermédiaires)s
        const filepath = join(__dirname, '../../../data/rates.json');
        return await fs.readFile(filepath, 'utf-8');
    }

    @Get('/currencies')
    @Header('Content-Type', 'application/json')
    async currencies() {
        const filepath = join(__dirname, '../../../data/currencies.json');
        return await fs.readFile(filepath, 'utf-8');
    }

  
}


